<?php
  session_start();
  include_once "dades.php";
  $USER = $_POST['usuari'];
  $PASS = $_POST['psw'];
  $CORRECTE =  isset($_POST['usuari'])
            && isset($_POST['psw'])
            && isset($usuaris[$USER])
            && $usuaris[$USER] == $PASS;
  if($CORRECTE) {
    $_SESSION['usuari'] = $USER;
    header("location:menu.php");
    setcookie("error_login", '');
  }else {
    header("location:formulari_login.php");
    $value = "Error Login";
    setcookie("error_login", $value);
  }
?>
