<?php
  if(isset($_GET['n'])){
    $numero = $_GET['n'];
    $valors = array();

    if (isPrimer($numero)) {
      echo "El número " . $numero . " és un número primer.";
    } else {
      echo "El número " . $numero . " no és un número primer i els seus divisors són l'1, ";
      // Es recorre l'array i s'escriuen els divisors
      foreach ($valors as $key => $value) {
        echo " el ". $value . ",";
      }
      echo " i ell mateix.";
    }
    echo '<a href="menu.php"><h3>Torna al menú</h3></a>';
  } else {
    header("location:formulari_login.php");
  }

  /**
   * Funció per trobar si és primer o no
   */
  function isPrimer($num) {
    $cont = 0;
    global $valors;
    // Recorre tots els números de l'1 fins el $num
    for ($i = 2; $i < $num; $i++) {
      if($num % $i == 0) {
        // Afegeix els divisors a l'array
        $cont++;
        array_push($valors, $i);
      }
    }
    // Comprova si es primer o no
    if($cont >= 2){
      return false;
    } else {
      return true;
    }
  }
?>
