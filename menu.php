<?php
  session_start();
  include_once "comprovar_usuari.php"
 ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Menu</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="estils.css">
  </head>
  <body>
    <?php
      echo '<h1>Benvingut ' . $_SESSION['usuari'] . '</h1>';
    ?>
    <h2>Menú</h2>
    <a href="comprovar_divisors.php"><h3>Comprovar divisors</h3></a>
    <a href="conjetura_collatz.php"><h3>Conjetura collatz</h3></a>
    <a href="sortir.php"><h3>Sortir</h3></a>
  </body>
</html>
