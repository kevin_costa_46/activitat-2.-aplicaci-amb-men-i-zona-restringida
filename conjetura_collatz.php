<?php
  session_start();
  include_once "comprovar_usuari.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Conjetura Collatz</title>
    <link rel="stylesheet" href="estils.css">
  </head>
  <body>
    <h1>Conjetura Collatz</h1>
    <form action="conjetura.php" method="get">
      Entra un número:<br><br>
      <input type="number" name="n" min="1"><br><br>
      <input type="submit" name="button" value="Calcular">
    </form>
    <br><a href="menu.php">Tornar al menú</a>
  </body>
</html>
