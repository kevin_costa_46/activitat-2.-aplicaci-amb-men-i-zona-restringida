<?php
    session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Login</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="estils.css">
  </head>
  <body>
    <h1>Login</h1>
    <?php
      if(isset($_COOKIE['error_login'])){
        echo '<p>' . $_COOKIE["error_login"] . '</p>';
      }
    ?>
    <form action="processa_login.php" method="post" id="form" name="formulari">
      Usuari:
      <br><input type="text" name="usuari"><br><br>
      Contrasenya:
      <br><input type="password" name="psw"><br><br>
      <button type="submit" id="submit">Login</button>
      <input type="reset" value="Reset">
    </form>
  </body>
</html>
