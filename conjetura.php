<?php
  if(isset($_GET['n'])) {
    $numero = $_GET['n'];
    while ($numero != 1) {
      if ($numero % 2 == 0) {
        $numero = $numero / 2;
      } else {
        $numero = $numero * 3 + 1;
      }
      echo $numero . ", ";
    }
    echo '<a href="menu.php"><h3>Torna al menú</h3></a>';
  } else {
    header("location:formulari_login.php");
  }
?>
