<?php
  session_start();
  include_once "comprovar_usuari.php";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="estils.css">
    <title>Comprovar Divisors</title>
  </head>
  <body>
    <h1>Comprovar els divisors d'un número</h1>

    <form action="divisors.php" method="get">
      Entra un número:<br><br>
      <input type="number" name="n" min="1"><br><br>
      <input type="submit" name="button" value="Enviar">
    </form>
    <br><a href="menu.php">Tornar al menú</a>
  </body>
</html>
